package backupmetos3

import (
	"context"
	"io"
	"net/http"
	"os"
	"syscall"
	"time"

	"gitlab.com/gopkgz/backupmetos3/pkg/errors"
	"gitlab.com/gopkgz/backupmetos3/pkg/healthchecks"
	"gitlab.com/gopkgz/backupmetos3/pkg/objectstorage"
	"gitlab.com/gopkgz/backupmetos3/pkg/secret"
)

const (
	errorPrefix = "backupmetos3: "
)

var (
	// ErrUserInput user input error, e.g. backupFn is nil
	ErrUserInput = errors.Newf(errorPrefix + "user input error")
)

type backuper struct {
	UID               int
	GID               int
	printf            func(format string, v ...any)
	HTTPClient        *http.Client // aws sdk needs http.Client
	HealthchecksUUID  string
	AWSBucketWithPath string
	AWSRegion         string
	AWSEndpoint       string
	AWSKey            secret.String
	AWsSecret         secret.String
	AWSRetryCount     int

	syscaller syscaller
}

// New creates a new backup runner.
func New(endpoint string, region string, bucketWithPath string, awsKey string, awsSecret string, awsRetryCount int, healthchecksUUID string, httpClient *http.Client, uid int, gid int, printfFn func(format string, v ...any)) *backuper {
	if printfFn == nil {
		printfFn = devNullPrintf
	}

	return &backuper{
		AWSRegion:         region,
		AWSEndpoint:       endpoint,
		AWSBucketWithPath: bucketWithPath,
		AWSKey:            secret.String{Value: awsKey},
		AWsSecret:         secret.String{Value: awsSecret},
		AWSRetryCount:     awsRetryCount,
		UID:               uid,
		GID:               gid,
		HealthchecksUUID:  healthchecksUUID,
		HTTPClient:        httpClient,
		printf:            printfFn,
		syscaller:         &osSyscaller{},
	}
}

// Run periodically runs the backup cycle with the given `backupFn` and `deleteStrategy`.
func (b *backuper) Run(ctx context.Context, backupFn func(context.Context, io.Writer) error, deleteStrategy func(string, time.Time) bool, rerunInterval time.Duration) error {
	if b.GID != 0 {
		if err := b.syscaller.Setgroups([]int{b.GID}); err != nil {
			return errors.Wrapf(err, "setgroups error, setting gid=%d", b.GID)
		}
	}
	if b.UID != 0 {
		if err := b.syscaller.Setuid(b.UID); err != nil {
			return errors.Wrapf(err, "setuid error, setting uid=%d", b.UID)
		}
	}

	if err := b.run(ctx, backupFn, deleteStrategy); err != nil {
		return err
	}

	b.printf("Next backup scheduled in %s", rerunInterval)
	t := time.NewTicker(rerunInterval)
	defer t.Stop()
	for {
		select {
		case <-t.C:
			if err := b.run(ctx, backupFn, deleteStrategy); err != nil {
				return err
			}
		case <-ctx.Done():
			return nil
		}
	}
}

func (b *backuper) run(ctx context.Context, backupFn func(context.Context, io.Writer) error, deleteStrategy func(string, time.Time) bool) error {
	if backupFn == nil || deleteStrategy == nil {
		return ErrUserInput
	}

	b.printf("Starting backup")

	f, err := b.syscaller.CreateTemp("", "backupmetos3")
	if err != nil {
		return errors.Wrap(err, "creating temp file")
	}
	defer func() {
		if err = f.Close(); err != nil {
			b.printf("Error closing temp file: %s", err)
			return
		}
		if err := os.Remove(f.Name()); err != nil {
			b.printf("Error removing temp file: %s", err)
		}
	}()

	b.printf("Backing up to %s", f.Name())
	if err := backupFn(ctx, f); err != nil {
		return err
	}

	b.printf("Uploading backup to s3:%s", b.AWSBucketWithPath)
	c := objectstorage.Client{
		BucketWithPath: b.AWSBucketWithPath,
		Key:            b.AWSKey,
		Secret:         b.AWsSecret,
		HTTPClient:     b.HTTPClient,
		Endpoint:       b.AWSEndpoint,
		Region:         b.AWSRegion,
		RetryCount:     b.AWSRetryCount,
	}
	if err := c.Upload(f); err != nil {
		return err
	}
	b.printf("Uploaded backup to s3:%s/%s", b.AWSBucketWithPath, f.Name())
	if err := c.Cleanup(deleteStrategy); err != nil {
		return err
	}
	b.printf("Cleaned up s3:%s", b.AWSBucketWithPath)

	if b.HealthchecksUUID != "" {
		b.printf("Reporting to healthchecks.io")
		if err := healthchecks.Beacon(ctx, b.HTTPClient, b.HealthchecksUUID); err != nil {
			return err
		}
	}

	b.printf("Backup complete")
	return nil
}

type readWriteSeekNameCloser interface {
	io.Writer
	io.Reader
	io.Seeker
	io.Closer
	Name() string
}

type syscaller interface {
	CreateTemp(dir, pattern string) (readWriteSeekNameCloser, error)
	Setgroups(gids []int) error
	Setuid(uid int) error
}

type osSyscaller struct {
}

func (osSyscaller) CreateTemp(dir, pattern string) (readWriteSeekNameCloser, error) {
	return os.CreateTemp(dir, pattern)
}

func (osSyscaller) Setgroups(gids []int) error {
	return syscall.Setgroups(gids)
}

func (osSyscaller) Setuid(uid int) error {
	return syscall.Setuid(uid)
}

func devNullPrintf(format string, v ...any) {}
