package backupmetos3

import (
	"context"
	"net/http"
	"testing"
	"time"
)

func TestSetGidUid(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	httpClient := &http.Client{
		Timeout: time.Second * 1,
	}

	syscaller := &testSyscaller{}

	uid := 42
	gid := 43
	bucket := "bucket/1/2/3"

	b := &backuper{
		UID:               uid,
		GID:               gid,
		printf:            devNullPrintf,
		HTTPClient:        httpClient,
		HealthchecksUUID:  "",
		AWSBucketWithPath: bucket,

		syscaller: syscaller,
	}

	if err := b.Run(ctx, nil, nil, 100*time.Second); err == nil {
		t.Errorf("expected error, got nil")
	}
	if syscaller.Uid != uid {
		t.Errorf("unexpected uid: %d", syscaller.Uid)
	}
	if syscaller.Gid != gid {
		t.Errorf("unexpected gid: %d", syscaller.Gid)
	}
}

type testSyscaller struct {
	Uid  int
	Gid  int
	file *testFile
}

func (s *testSyscaller) Setgroups(groups []int) error {
	s.Gid = groups[0]
	return nil
}

func (s *testSyscaller) Setuid(uid int) error {
	s.Uid = uid
	return nil
}

func (s *testSyscaller) CreateTemp(dir, pattern string) (readWriteSeekNameCloser, error) {
	return s.file, nil
}

type testFile struct {
	realFile    readWriteSeekNameCloser
	closeCalled bool
	writeCalled bool
	readCalled  bool
	seekCalled  bool
}

func (f *testFile) Read(p []byte) (int, error) {
	f.readCalled = true
	return f.realFile.Read(p)
}

func (f *testFile) Write(p []byte) (int, error) {
	f.writeCalled = true
	return f.realFile.Write(p)
}

func (f *testFile) Close() error {
	f.closeCalled = true
	return f.realFile.Close()
}

func (f *testFile) Name() string {
	return f.realFile.Name()
}

func (f *testFile) Seek(offset int64, whence int) (int64, error) {
	f.seekCalled = true
	return f.realFile.Seek(offset, whence)
}
