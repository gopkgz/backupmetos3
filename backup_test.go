package backupmetos3_test

import (
	"context"
	"io"
	"net/http"
	"testing"
	"time"

	"gitlab.com/gopkgz/backupmetos3"
)

func devNullPrintf(format string, v ...any) {}

func TestRunFunc(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
	defer cancel()

	httpClient := &http.Client{
		Timeout: time.Second * 1,
	}

	bucket := "bucket/1/2/3"

	endpoint := ""
	region := ""
	awsKey := ""
	awsSecret := ""
	retries := 0
	healthchecksUUID := ""

	testCases := []struct {
		name             string
		backupFn         func(context.Context, io.Writer) error
		deleteStrategyFn func(string, time.Time) bool
		expectedError    error
	}{
		{"nil backupFn", nil, backupmetos3.DefaultDeleteStrategy(), backupmetos3.ErrUserInput},
		{"nil backupFn", nil, nil, backupmetos3.ErrUserInput},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			b := backupmetos3.New(endpoint, region, bucket, awsKey, awsSecret, retries, healthchecksUUID, httpClient, 0, 0, devNullPrintf)

			if err := b.Run(ctx, tc.backupFn, tc.deleteStrategyFn, 100*time.Second); err != tc.expectedError {
				t.Errorf("unexpected error: %v", err)
			}
		})
	}
}
