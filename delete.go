package backupmetos3

import (
	"fmt"
	"time"
)

const (
	day             = 24 * time.Hour
	week            = 7 * day
	month           = 30 * day  // good enough
	year            = 365 * day // good enough
	keepDailysFor   = 7 * day
	keepWeeklysFor  = 8 * week
	keepMonthlysFor = 12 * month
	keepYearlysFor  = 3 * year
)

// DefaultDeleteStrategy creates a function that decides on archive pruning and deletion.
// Keeps daily archives for `keepDailysFor`
// Keeps weekly archives for `keepWeeklysFor`
// Keeps monthly archives for `keepMonthlysFor`. Note: month = 30 days, not calendar month
// Keeps yearly archives for `keepYearlysFor`. Note: year = 365 days, not calendar year
func DefaultDeleteStrategy() func(k string, l time.Time) bool {
	d := &defaultStrategy{
		dailys:   make(map[string]bool),
		weeklys:  make(map[string]bool),
		monthlys: make(map[string]bool),
		yearlys:  make(map[string]bool),
		now:      time.Now(),
	}
	return d.Decider
}

type defaultStrategy struct {
	dailys   map[string]bool
	weeklys  map[string]bool
	monthlys map[string]bool
	yearlys  map[string]bool
	now      time.Time
}

func (s *defaultStrategy) Decider(k string, l time.Time) bool {
	if l.Add(keepDailysFor).After(s.now) {
		if _, ok := s.dailys[dayKey(l)]; ok {
			return true
		}
		s.dailys[dayKey(l)] = true
		return false
	}
	if l.Add(keepWeeklysFor).After(s.now) {
		if _, ok := s.weeklys[weekKey(l)]; ok {
			return true
		}
		s.weeklys[weekKey(l)] = true
		return false
	}
	if l.Add(keepMonthlysFor).After(s.now) {
		if _, ok := s.monthlys[monthKey(l)]; ok {
			return true
		}
		s.monthlys[monthKey(l)] = true
		return false
	}
	if l.Add(keepYearlysFor).After(s.now) {
		if _, ok := s.yearlys[yearKey(l)]; ok {
			return true
		}
		s.yearlys[yearKey(l)] = true
		return false
	}
	return true
}

func dayKey(t time.Time) string {
	return fmt.Sprintf("%d-%d-%d", t.Year(), t.Month(), t.Day())
}

func weekKey(t time.Time) string {
	y, w := t.ISOWeek()
	return fmt.Sprintf("%d-%d", y, w)
}

func monthKey(t time.Time) string {
	return fmt.Sprintf("%d-%d", t.Year(), t.Month())
}

func yearKey(t time.Time) string {
	return fmt.Sprintf("%d", t.Year())
}
