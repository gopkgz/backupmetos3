package backupmetos3

import (
	"testing"
	"time"
)

func TestDefaultStrategy(t *testing.T) {
	now := time.Now()
	fixedDate := time.Date(2022, time.February, 25, 1, 0, 0, 0, time.UTC)
	fixedMonth := time.Date(2022, time.February, 1, 1, 0, 0, 0, time.UTC)

	type args struct {
		lastModifiedTime time.Time
		delete           bool
	}

	testCases := []struct {
		name   string
		now    time.Time
		values []args
	}{
		{
			"too old for yearly retention",
			now,
			[]args{
				{
					lastModifiedTime: now.Add(-2 * keepYearlysFor),
					delete:           true,
				},
				{
					lastModifiedTime: now.Add(-keepYearlysFor - 1*time.Hour),
					delete:           true,
				},
				{
					lastModifiedTime: now.Add(-keepYearlysFor - 1*time.Second),
					delete:           true,
				},
			},
		},
		{
			"keep 1 yearly",
			fixedDate,
			[]args{
				{
					lastModifiedTime: fixedDate.Add(-keepYearlysFor + 1*time.Hour),
					delete:           false,
				},
				{
					lastModifiedTime: fixedDate.Add(-keepYearlysFor + day),
					delete:           true,
				},
				{
					lastModifiedTime: fixedDate.Add(-keepYearlysFor + week),
					delete:           true,
				},
				{
					lastModifiedTime: fixedDate.Add(-keepYearlysFor + 4*month),
					delete:           true,
				},
			},
		},
		{
			"keep 1 monthly",
			fixedDate,
			[]args{
				{
					lastModifiedTime: fixedMonth.Add(-6*month + 1*time.Hour),
					delete:           false,
				},
				{
					lastModifiedTime: fixedMonth.Add(-6*month + 1*day),
					delete:           true,
				},
				{
					lastModifiedTime: fixedMonth.Add(-6*month + 1*week),
					delete:           true,
				},
				{
					lastModifiedTime: fixedMonth.Add(-6*month + 3*week),
					delete:           true,
				},
			},
		},
		{
			"keep 1 weekly",
			fixedDate,
			[]args{
				{
					lastModifiedTime: fixedMonth.Add(-1 * time.Minute),
					delete:           false,
				},
				{
					lastModifiedTime: fixedMonth.Add(30 * time.Minute),
					delete:           true,
				},
				{
					lastModifiedTime: fixedMonth.Add(1 * day),
					delete:           true,
				},
				{
					lastModifiedTime: fixedMonth.Add(1*day + 1*time.Hour),
					delete:           true,
				},
			},
		},
		{
			"keep 1 daily",
			fixedDate,
			[]args{
				{
					lastModifiedTime: fixedDate.Add(-1 * time.Minute),
					delete:           false,
				},
				{
					lastModifiedTime: fixedDate.Add(30 * time.Minute),
					delete:           true,
				},
				{
					lastModifiedTime: fixedDate.Add(4 * time.Hour),
					delete:           true,
				},
				{
					lastModifiedTime: fixedDate.Add(1 * day),
					delete:           false,
				},
				{
					lastModifiedTime: fixedDate.Add(1*day + 1*time.Hour),
					delete:           true,
				},
				{
					lastModifiedTime: fixedDate.Add(1*day + 4*time.Hour),
					delete:           true,
				},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			strategy := defaultStrategy{
				now:      tc.now,
				dailys:   make(map[string]bool),
				weeklys:  make(map[string]bool),
				monthlys: make(map[string]bool),
				yearlys:  make(map[string]bool),
			}

			for i, v := range tc.values {
				if v.delete != strategy.Decider("does/not/matter", v.lastModifiedTime) {
					t.Errorf("%s[%d] expected %t, got %t", tc.name, i, v.delete, !v.delete)
				}
			}
		})
	}
}

func TestDefaultStrategyCreation(t *testing.T) {
	decider := DefaultDeleteStrategy()
	if decider == nil {
		t.Error("DefaultDeleteStrategy() returned nil")
	}
}
