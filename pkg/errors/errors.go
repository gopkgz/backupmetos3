package errors

import "fmt"

// Newf creates a new error with the given `fmt` message.
func Newf(message string, args ...interface{}) error {
	return fmt.Errorf(message, args...)
}

// Wrap wraps the given error with the given message.
func Wrap(err error, message string) error {
	return fmt.Errorf("%s: %w", message, err)
}

// Wrapf wraps the given error with the given `fmt` message.
func Wrapf(err error, format string, args ...interface{}) error {
	formatArgs := make([]interface{}, len(args)+1)
	copy(formatArgs, args)
	formatArgs[len(args)] = err

	return fmt.Errorf(format+": %w", formatArgs...)
}
