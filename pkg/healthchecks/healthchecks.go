package healthchecks

import (
	"context"
	"net/http"

	"gitlab.com/gopkgz/backupmetos3/pkg/errors"
)

const (
	hcPingUrl = "https://hc-ping.com/"
)

type doer interface {
	Do(req *http.Request) (*http.Response, error)
}

// Beacon send healthcheck.io "ping" for the given cronjob.
func Beacon(ctx context.Context, client doer, uuid string) error {
	req, err := http.NewRequest("HEAD", hcPingUrl+uuid, nil)
	if err != nil {
		return err
	}
	var resp *http.Response
	if resp, err = client.Do(req); err != nil {
		return err
	}
	if resp.StatusCode != http.StatusOK {
		return errors.Newf("healthchecks.io: HTTP " + resp.Status + ": " + http.StatusText(resp.StatusCode))
	}
	return nil
}
