package healthchecks_test

import (
	"context"
	"net/http"
	"testing"

	"gitlab.com/gopkgz/backupmetos3/pkg/healthchecks"
)

type testHttpClient struct {
	Url string
}

func (c *testHttpClient) Do(req *http.Request) (*http.Response, error) {
	c.Url = req.URL.String()

	return &http.Response{
		StatusCode: http.StatusOK,
	}, nil
}

func TestBeacon(t *testing.T) {
	client := &testHttpClient{}
	if err := healthchecks.Beacon(context.Background(), client, "uuid"); err != nil {
		t.Error(err)
	}
	if client.Url != "https://hc-ping.com/uuid" {
		t.Errorf("Beacon() should have called client.Head() with the correct URL")
	}
}
