package objectstorage

import (
	"path"
	"strings"
)

func splitBucketPath(bucketWithPath string) (string, string) {
	res := strings.SplitN(bucketWithPath, "/", 2)
	bucket := res[0]
	if len(res) == 1 {
		return bucket, ""
	}
	bucketPath := res[1]
	if len(bucketPath) > 0 && bucketPath[len(bucketPath)-1] == byte('/') {
		bucketPath = bucketPath[:len(bucketPath)-1] // remove trailing slash
	}
	return bucket, bucketPath
}

func joinBucketPath(basePath string, fileName string) string {
	if basePath == "" && fileName == "" {
		return ""
	}
	if basePath == "" {
		return fileName
	}
	return path.Join(basePath, fileName)
}
