package objectstorage

import "testing"

func TestSplitBucket(t *testing.T) {
	testCases := []struct{ in, bucket, path string }{
		{"bucket1", "bucket1", ""},
		{"bucket1/", "bucket1", ""},
		{"bucket1/path1", "bucket1", "path1"},
		{"bucket1/path1/", "bucket1", "path1"},
		{"bucket1/path1/path2", "bucket1", "path1/path2"},
	}

	for _, tc := range testCases {
		t.Run(tc.in, func(t *testing.T) {
			bucket, path := splitBucketPath(tc.in)
			if bucket != tc.bucket {
				t.Errorf("expected bucket %s, got %s", tc.bucket, bucket)
			}
			if path != tc.path {
				t.Errorf("expected path %s, got %s", tc.path, path)
			}
		})
	}
}

func TestJoinBucket(t *testing.T) {
	testCases := []struct{ bucket, path, expected string }{
		{"", "", ""},
		{"bucketpath1", "", "bucketpath1"},
		{"", "myfile.abc", "myfile.abc"},
		{"bucketpath1/", "nested/filepath/myfile.abc", "bucketpath1/nested/filepath/myfile.abc"},
		{"bucketpath1", "myfile.abc", "bucketpath1/myfile.abc"},
		{"bucketpath1/a/b/c", "myfile.abc", "bucketpath1/a/b/c/myfile.abc"},
		{"bucketpath1/a/b/", "myfile.abc", "bucketpath1/a/b/myfile.abc"},
		{"bucketpath1/a/b", "nested/filepath/myfile.abc", "bucketpath1/a/b/nested/filepath/myfile.abc"},
	}

	for _, tc := range testCases {
		t.Run(tc.expected, func(t *testing.T) {
			actual := joinBucketPath(tc.bucket, tc.path)
			if actual != tc.expected {
				t.Errorf("expected %s, got %s", tc.expected, actual)
			}
		})
	}
}
