package objectstorage

import (
	"io"
	"net/http"
	"reflect"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"gitlab.com/gopkgz/backupmetos3/pkg/errors"
	"gitlab.com/gopkgz/backupmetos3/pkg/secret"
)

const (
	minRetryDelay = 5 * time.Second
	errorPrefix   = "objectstorage: "
)

var (
	errBucketParamEmpty   = errors.Newf(errorPrefix + "bucket with path is empty")
	errFileNameParamEmpty = errors.Newf(errorPrefix + "file name is empty")
	errFileParamNil       = errors.Newf(errorPrefix + "file is nil")
)

type NamedReadSeeker interface {
	io.ReadSeeker
	Name() string
}

type Client struct {
	Region         string
	Endpoint       string
	BucketWithPath string
	Key            secret.String
	Secret         secret.String
	RetryCount     int
	HTTPClient     *http.Client
}

func (c *Client) configHttpClient() *http.Client {
	if c.HTTPClient != nil {
		return c.HTTPClient
	}
	return http.DefaultClient
}

func (c *Client) awsConfig() *aws.Config {
	credentialsToken := ""

	cfg := &aws.Config{
		S3ForcePathStyle: aws.Bool(true),
		Credentials:      credentials.NewStaticCredentials(c.Key.Value, c.Secret.Value, credentialsToken),
		HTTPClient:       c.configHttpClient(),
	}
	if c.Endpoint != "" {
		cfg.Endpoint = aws.String(c.Endpoint)
	}
	if c.Region != "" {
		cfg.Region = aws.String(c.Region)
	}
	if c.RetryCount > 0 {
		cfg.MaxRetries = aws.Int(c.RetryCount)
		cfg.Retryer = client.DefaultRetryer{NumMaxRetries: c.RetryCount, MinRetryDelay: minRetryDelay}
	}
	return cfg
}

// Upload upload the given file to s3
func (c *Client) Upload(file NamedReadSeeker) error {
	goSession, err := session.NewSessionWithOptions(session.Options{
		Config: *c.awsConfig(),
	})
	if err != nil {
		return errors.Wrap(err, errorPrefix+"failed to create session")
	}

	s3Client := s3.New(goSession)
	return putObject(s3Client, c.BucketWithPath, file)
}

// Cleanup delete objects from S3 bucket path according to the given delete strategy.
func (c *Client) Cleanup(deleteStrategy func(key string, lastModifiedTime time.Time) bool) error {
	goSession, err := session.NewSessionWithOptions(session.Options{
		Config: *c.awsConfig(),
	})
	if err != nil {
		return errors.Wrap(err, errorPrefix+"failed to create session")
	}

	s3Client := s3.New(goSession)
	return cleanup(s3Client, c.BucketWithPath, deleteStrategy)
}

type s3ObjectPutter interface {
	PutObject(input *s3.PutObjectInput) (*s3.PutObjectOutput, error)
}

func putObject(s3Client s3ObjectPutter, bucketWithPath string, file NamedReadSeeker) error {
	if bucketWithPath == "" {
		return errBucketParamEmpty
	}
	if isNil(file) {
		return errFileParamNil
	}
	if file.Name() == "" {
		return errFileNameParamEmpty
	}

	bucket, bucketBasePath := splitBucketPath(bucketWithPath)

	putObjectInput := &s3.PutObjectInput{
		Body:   file,
		Bucket: aws.String(bucket),
		Key:    aws.String(joinBucketPath(bucketBasePath, file.Name())),
	}
	if _, err := s3Client.PutObject(putObjectInput); err != nil {
		return errors.Wrap(err, errorPrefix+"failed to put object")
	}
	return nil
}

type s3ObjectCleaner interface {
	DeleteObject(input *s3.DeleteObjectInput) (*s3.DeleteObjectOutput, error)
	ListObjects(input *s3.ListObjectsInput) (*s3.ListObjectsOutput, error)
}

func cleanup(s3Client s3ObjectCleaner, bucketWithPath string, deleteStrategy func(string, time.Time) bool) error {
	bucket, bucketBasePath := splitBucketPath(bucketWithPath)

	listReq := s3.ListObjectsInput{
		Bucket: aws.String(bucket),
	}
	if bucketBasePath != "" {
		listReq.Prefix = aws.String(bucketBasePath + "/")
	}
	listResp, err := s3Client.ListObjects(&listReq)
	if err != nil {
		return errors.Wrap(err, errorPrefix+"failed to list objects")
	}

	for _, object := range listResp.Contents {
		if deleteStrategy(*object.Key, *object.LastModified) {
			_, err := s3Client.DeleteObject(&s3.DeleteObjectInput{
				Bucket: aws.String(bucket),
				Key:    object.Key,
			})
			if err != nil {
				return errors.Wrap(err, errorPrefix+"failed to delete object")
			}
		}
	}
	return nil
}

func isNil(i interface{}) bool {
	return i == nil || reflect.ValueOf(i).IsNil()
}
