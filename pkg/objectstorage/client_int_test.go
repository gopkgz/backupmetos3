package objectstorage

import (
	"bytes"
	"fmt"
	"net/http"
	"strconv"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/service/s3"
	"gitlab.com/gopkgz/backupmetos3/pkg/secret"
)

const (
	day  = 24 * time.Hour
	year = 365 * day // good enough
)

func TestCleanup(t *testing.T) {
	c := &testS3ObjectCleaner{
		objects: []*s3.Object{
			{
				Key:          aws.String("test-bucket/with/path/test1"),
				LastModified: aws.Time(time.Now().Add(-1 * time.Hour)),
			},
			{
				Key:          aws.String("test-bucket/with/path/test2"),
				LastModified: aws.Time(time.Now().Add(-100 * day)),
			},
			{
				Key:          aws.String("test-bucket/with/path/test3"),
				LastModified: aws.Time(time.Now().Add(-100 * year)),
			},
		},
	}
	if err := cleanup(c, "test-bucket/with/path", deleteAllPast(10*time.Minute)); err != nil {
		t.Error(err)
	}
	if len(c.deletedObjects) != len(c.objects) {
		for i, key := range c.deletedObjects {
			if key != *c.objects[i].Key {
				t.Errorf("expected %s to be deleted, got %s", *c.objects[i].Key, key)
			}
		}
	}
}

func TestPutObject(t *testing.T) {
	testCases := []struct {
		name           string
		bucketWithPath string
		inputFileName  string
		expectedError  error
	}{
		{"empty bucket", "", "file.123", errBucketParamEmpty},
		{"empty filepath", "test-bucket", "", errFileNameParamEmpty},
		{"test file in root", "bucket", "mybackup-123.zip", nil},
		{"test nested filepath", "bucket", "nested/path/mybackup-123.zip", nil},
		{"test bucket prefix", "bucket/prefix", "mybackup-123.zip", nil},
		{"test bucket prefix with nested file path", "bucket/prefix", "nested/path/mybackup-123.zip", nil},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			fk := fakeFile{
				name: tc.inputFileName,
			}

			c := &testS3ObjectPutter{}
			err := putObject(c, tc.bucketWithPath, &fk)
			if err != nil {
				if err != tc.expectedError {
					t.Errorf("expected error %v, got %v", tc.expectedError, err)
				}
				return
			}
			_, bucketPath := splitBucketPath(tc.bucketWithPath)
			if bucketPath == "" {
				if c.objects[0] != tc.inputFileName {
					t.Errorf("expected %s to be uploaded, got %s", tc.inputFileName, c.objects[0])
				}
				return
			}
			if c.objects[0] != bucketPath+"/"+tc.inputFileName {
				t.Errorf("expected %s to be uploaded, got %s", bucketPath+"/"+tc.inputFileName, c.objects[0])
			}
		})
	}
}

func TestFileIsNil(t *testing.T) {
	var f *fakeFile

	c := &testS3ObjectPutter{}
	if err := putObject(c, "test-bucket", f); err != errFileParamNil {
		t.Errorf("expected error %v, got %v", errFileParamNil, err)
	}
}

func TestAWSConfig(t *testing.T) {
	httpClient := &http.Client{
		Timeout: time.Second,
	}

	testCases := []struct {
		name           string
		endpoint       string
		region         string
		bucketWithPath string
		key            string
		secret         string
		retryCount     int
		httpClient     *http.Client
		expectedError  error
		expectedResult *aws.Config
	}{
		{"default params", "", "", "", "suchsecret", "verysecretwow", 0, nil, nil, &aws.Config{
			S3ForcePathStyle: aws.Bool(true),
			Credentials:      credentials.NewStaticCredentials("suchsecret", "verysecretwow", ""),
			HTTPClient:       http.DefaultClient,
			Endpoint:         nil,
			Region:           nil,
			MaxRetries:       nil,
			Retryer:          nil,
		}},
		{"no auth", "", "", "", "", "", 0, nil, credentials.ErrStaticCredentialsEmpty, &aws.Config{
			S3ForcePathStyle: aws.Bool(true),
			Credentials:      nil,
			HTTPClient:       http.DefaultClient,
			Endpoint:         nil,
			Region:           nil,
			MaxRetries:       nil,
			Retryer:          nil,
		}},
		{"retry", "", "", "", "suchsecret", "verysecretwow", 42, nil, nil, &aws.Config{
			S3ForcePathStyle: aws.Bool(true),
			Credentials:      credentials.NewStaticCredentials("suchsecret", "verysecretwow", ""),
			HTTPClient:       http.DefaultClient,
			Endpoint:         nil,
			Region:           nil,
			MaxRetries:       aws.Int(42),
			Retryer:          client.DefaultRetryer{NumMaxRetries: 42, MinRetryDelay: minRetryDelay},
		}},
		{"endpoint + region", "https://s3.example.com", "us-north4", "", "suchsecret", "verysecretwow", 0, nil, nil, &aws.Config{
			S3ForcePathStyle: aws.Bool(true),
			Credentials:      credentials.NewStaticCredentials("suchsecret", "verysecretwow", ""),
			HTTPClient:       http.DefaultClient,
			Endpoint:         aws.String("https://s3.example.com"),
			Region:           aws.String("us-north4"),
			MaxRetries:       nil,
			Retryer:          nil,
		}},
		{"http.Client", "", "", "", "suchsecret", "verysecretwow", 0, httpClient, nil, &aws.Config{
			S3ForcePathStyle: aws.Bool(true),
			Credentials:      credentials.NewStaticCredentials("suchsecret", "verysecretwow", ""),
			HTTPClient:       httpClient,
			Endpoint:         nil,
			Region:           nil,
			MaxRetries:       nil,
			Retryer:          nil,
		}},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			c := Client{
				BucketWithPath: tc.bucketWithPath,
				Key:            secret.String{Value: tc.key},
				Secret:         secret.String{Value: tc.secret},
				HTTPClient:     tc.httpClient,
				Endpoint:       tc.endpoint,
				Region:         tc.region,
				RetryCount:     tc.retryCount,
			}
			config := c.awsConfig()
			if err := compareAWSConfig(t, config, tc.expectedResult); err != tc.expectedError {
				t.Error(err)
			}
		})
	}
}

func compareAWSConfig(t *testing.T, actual *aws.Config, expected *aws.Config) error {
	t.Helper()

	if actual == nil && expected == nil {
		return nil
	}
	if actual == nil && expected != nil {
		return fmt.Errorf("expected non-nil aws.Config, got nil")
	}
	if expected == nil && actual != nil {
		return fmt.Errorf("expected nil aws.Config, got non-nil")
	}

	if !equalStrPtr(t, actual.Endpoint, expected.Endpoint) {
		return fmt.Errorf("expected endpoint %s, got %s", fmtStrPtr(expected.Endpoint), fmtStrPtr(actual.Endpoint))
	}
	if !equalStrPtr(t, actual.Region, expected.Region) {
		return fmt.Errorf("expected region %s, got %s", fmtStrPtr(expected.Region), fmtStrPtr(actual.Region))
	}
	actualCreds, err := actual.Credentials.Get()
	if err != nil {
		return err
	}
	expectedCreds, err := expected.Credentials.Get()
	if err != nil {
		return err
	}
	if actualCreds.AccessKeyID != expectedCreds.AccessKeyID {
		return fmt.Errorf("expected access key id %s, got %s", expectedCreds.AccessKeyID, actualCreds.AccessKeyID)
	}
	if actualCreds.SecretAccessKey != expectedCreds.SecretAccessKey {
		return fmt.Errorf("expected secret access key %s, got %s", expectedCreds.SecretAccessKey, actualCreds.SecretAccessKey)
	}
	if actualCreds.SessionToken != expectedCreds.SessionToken {
		return fmt.Errorf("expected session token %s, got %s", expectedCreds.SessionToken, actualCreds.SessionToken)
	}
	if actual.HTTPClient != expected.HTTPClient {
		return fmt.Errorf("expected http client %v, got %v", expected.HTTPClient, actual.HTTPClient)
	}
	if actual.Retryer != expected.Retryer {
		return fmt.Errorf("expected retryer %v, got %v", expected.Retryer, actual.Retryer)
	}
	if !equalIntPtr(t, actual.MaxRetries, expected.MaxRetries) {
		return fmt.Errorf("expected max retries %s, got %s", fmtIntPtr(expected.MaxRetries), fmtIntPtr(actual.MaxRetries))
	}
	return nil
}

func equalStrPtr(t *testing.T, actual *string, expected *string) bool {
	t.Helper()

	if actual == nil || expected == nil {
		return actual == expected
	}
	return *actual == *expected
}

func equalIntPtr(t *testing.T, actual *int, expected *int) bool {
	t.Helper()

	if actual == nil || expected == nil {
		return actual == expected
	}
	return *actual == *expected
}

func fmtStrPtr(s *string) string {
	if s == nil {
		return "<nil>"
	}
	if *s == "" {
		return "''"
	}
	return *s
}

func fmtIntPtr(i *int) string {
	if i == nil {
		return "<nil>"
	}
	return strconv.Itoa(*i)
}

type fakeFile struct {
	bytes.Buffer
	name string
}

func (f *fakeFile) Name() string {
	return f.name
}

func (f *fakeFile) Seek(offset int64, whence int) (int64, error) {
	return 0, nil
}

type testS3ObjectPutter struct {
	objects []string
}

func (s *testS3ObjectPutter) PutObject(input *s3.PutObjectInput) (*s3.PutObjectOutput, error) {
	s.objects = append(s.objects, *input.Key)
	return nil, nil
}

type testS3ObjectCleaner struct {
	objects        []*s3.Object
	deletedObjects []string
}

func (s *testS3ObjectCleaner) DeleteObject(input *s3.DeleteObjectInput) (*s3.DeleteObjectOutput, error) {
	s.deletedObjects = append(s.deletedObjects, *input.Key)
	return nil, nil
}

func (s *testS3ObjectCleaner) ListObjects(input *s3.ListObjectsInput) (*s3.ListObjectsOutput, error) {
	return &s3.ListObjectsOutput{
		Contents: s.objects,
	}, nil
}

func deleteAllPast(t time.Duration) func(string, time.Time) bool {
	return func(k string, l time.Time) bool {
		return l.Before(time.Now().Add(-t))
	}
}
