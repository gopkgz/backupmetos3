package secret

import "strings"

// String encapsulates a string value, but does not allow to print it accidentally.
type String struct {
	Value string
}

// String returns masked value. Use String.Value to get raw value.
func (s String) String() string {
	if len(s.Value) < 12 {
		return strings.Repeat("*", len(s.Value))
	}

	prefix := s.Value[:4]
	return prefix + strings.Repeat("*", len(s.Value[4:]))
}
