package secret_test

import (
	"fmt"
	"testing"

	"gitlab.com/gopkgz/backupmetos3/pkg/secret"
)

func TestSecret(t *testing.T) {
	testCases := []struct {
		name     string
		input    string
		expected string
	}{
		{"empty", "", ""},
		{"strlen=3", "abc", "***"},
		{"strlen=4", "abcd", "****"},
		{"strlen=5", "abcde", "*****"},
		{"strlen=11", "abcdabcdabc", "***********"},
		{"strlen=12", "abcdabcdabcd", "abcd********"},
		{"strlen=13", "abcdabcdabcda", "abcd*********"},
		{"long", "hello-world-secret-much", "hell*******************"},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			s := secret.String{tc.input}
			if s.Value != tc.input {
				t.Errorf("expected %s, got %s", tc.input, s.Value)
			}
			if s.String() != tc.expected {
				t.Errorf("expected %s, got %s", tc.expected, s.String())
			}
			printed := fmt.Sprint(s)
			if printed != tc.expected {
				t.Errorf("fmt.Sprint test case error, expected %s, got %s", tc.expected, printed)
			}
			printed = fmt.Sprintf("%v", s)
			if printed != tc.expected {
				t.Errorf("fmt.Sprintf %%v test case error, expected %s, got %s", tc.expected, printed)
			}
			printed = fmt.Sprintf("%+v", s)
			if printed != tc.expected {
				t.Errorf("fmt.Sprintf %%+v test case error, expected %s, got %s", tc.expected, printed)
			}
		})
	}
}
